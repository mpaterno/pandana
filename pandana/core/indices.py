""" indices

Index levels used to slice dataframes.
"""

KL = ["run", "subrun", "cycle", "evt", "subevt"]
KLN = ["run", "subrun", "cycle", "evt"]
KLS = ["run", "subrun", "evt"]
